## IHA Project

Indicators of Hydrologic Alteration (IHA) is a software developed by The Nature Conservancy (TNC). It's a tool that calculates the characteristics of natural and altered hydrologic regimes. The software has not been updated since 2009 and can only be used on PC computers. 

The goal of this project is to create a tool that will determine the Environmental Flow Components (EFC) described in IHA: 
- high flow pulse, 
- small flood minimum peak flow, 
- large flood minimum peak flow, 
- extreme low flow and 
- low flow 

for a selected USGS gage in the US using Python. Each gage selection will produce the standard IHA figure that describes EFC classes over a period of time.

IHA EFC figures are a standard in flow analysis, but using the IHA software can be bothersome to the user. The creation of this tool will ideally make the process of retrieving these figures smoother and more enjoyable for the user.  

Example of IHA standard figure output:
![Recreating this figure](/assets/IHA_EFC_fig.png)



